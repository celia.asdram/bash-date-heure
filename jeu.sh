#!/bin/bash

number=$(( $RANDOM % 1000 + 1 ))

guess=0
n=0

echo "Deviner le nombre compris entre 1 et 1000"

while [ "$guess" -ne $number ] ; do

        read -p " >  " guess

        [ "$guess" -lt $number ] && echo " $guess est trop petit"
        [ "$guess" -gt $number ] &&  echo " $guess est trop grand"

        n=$(($n+1))

done

echo " Bravo ! Vous avez trouvé le nombre en $n essais !"

exit 0
